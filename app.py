#!/home/developer/projeto/venv/bin/python3

from flask import Flask, render_template,redirect, request, session
from modules.bdocker import docker
from modules.bjenkins import jenkins
from modules.bgitlab import gitlab
from ldap3 import Server, Connection
import logging 
import os

# logging.basicConfig(
#     filename = 'app.log',
#     level = logging.DEBUG,
#     format = "%(asctime)s [%(levelname)s] %(name)s\n" + 
#             "[%(funcName)s] [%(filename)s. %(linemo)s] %(message)s",
#     datefmt= "[ %d-%m-%Y %H:%M:%S ]"
# )


# CUSTOM = 49

# logging.addLevelName(CUSTOM, "CUSTOM")
# def alert (self,message, *args, **kwargs):
#     if self.isEnabldFor(CUSTOM):
#         self._log(CUSTOM, message,args, kwargs)


app = Flask(__name__)
app.register_blueprint(docker)
app.register_blueprint(jenkins)
app.register_blueprint(gitlab)
#app.register_blueprint(login)


@app.route('/')
def index():
    return render_template('index.html')

@app.route('/login', methods=['POST']) 
def logar ():
    data = request.form
    print(data)
    server = Server('ldap://127.0.0.1')
    con = Connection(server, "uid={}, dc=dexter, dc=com, dc=br".format(data['email']),data['password'])
    if(con.bind()): 
        session['auth'] = True 
        return render_template('/index.html',result='True')       
    else:
        return render_template('/index.html',result='False')

@app.route('/deslogar')
def deslogar(): 
    session['auth'] = False
    return redirect ('/')

if __name__ == "__main__":
    app.secret_key = os.urandom(12)
    app.run(host='0.0.0.0', port=5000, debug=True)