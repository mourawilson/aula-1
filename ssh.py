import paramiko

client = paramiko.client.SSHClient()
client.load_system_host_keys()
client.set_missing_host_key_policy(paramiko.AutoAddPolicy())

#string de conexao 
client.connect(
    '192.168.205.3',
    username='sabado',
    password='sabado'
)

#comando a ser excutado, parametro de entrada e saida e erro
stdin, stdout, stderr = client.exec_command('ls -la')

#trata saida da execução de execução, caso sucesso retorna 0
if stdout.channel.recv_exit_status()==0:
    print(stdout.read().decode('utf-8'))
else:
    print(stderr.read().decode('utf-8'))