from flask import Blueprint, render_template, redirect
from docker import DockerClient


#client = docker.APIClient(base_url='unix://var/run/docker.sock')


try:
    con = DockerClient('tcp://127.0.0.1:2376')
    print(con)
except Exception as e:
        print('Erro: {}'.format(e))

        
docker = Blueprint('docker',__name__, url_prefix ="/docker")

@docker.route('/')
def index():
    containers = con.containers.list(all=True)
    #container = con.containers.get('aula')
    return render_template('docker.html',ctrs=containers)

@docker.route('/start/<id>')
def start(id):
    if(id):
        con.containers.get(id).start()
        return redirect('/docker')
        #return render_template('docker.html',result='Container Inciado',ctrs=containers)
    else:    
        return redirect('/docker')

@docker.route('/stop/<id>')
def stop(id):
    if(id):
        con.containers.get(id).stop()
        return redirect('/docker')
        #return render_template('docker.html',result='Container Parado',ctrs=containers)
    else:    
        return redirect('/docker')
