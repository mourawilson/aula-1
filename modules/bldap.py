from flask import Blueprint, render_template, redirect
from ldap3 import Server
import requests
from pprint import pprint

login = Blueprint('login', __name__, url_prefix= "/login")

@login.route('/')
def index():
    return redirect("/")