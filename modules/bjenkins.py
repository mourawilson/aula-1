from flask import Blueprint, render_template, redirect, request
from jenkins import Jenkins
from pprint import pprint



jenkins = Blueprint('jenkins',__name__, url_prefix="/jenkins")

con = Jenkins('http://127.0.0.1:8080', username = 'root', password='4linux')

@jenkins.route("/")
def index():
    return render_template("jenkins.html",jobs=con.get_all_jobs())

@jenkins.route('/build/<job>')
def build_job(job):
    con.build_job(job)
    return redirect('/jenkins')

@jenkins.route('/update/<job>')
def update_job(job):
    return render_template('jenkins_update.html',job=job, job_config=con.get_job_config(job)) 


@jenkins.route('/reconfig/<job>', methods=['POST'])
def reconfig_job(job):
    print (request.form['xml'])
    con.reconfig_job(job,request.form['xml'])
    return redirect('/jenkins')